from io import BytesIO, StringIO
import re
from tempfile import NamedTemporaryFile
import zipfile
import numpy as np
import pandas as pd
from rdkit import Chem
from rdkit.Chem import AllChem
import gzip as gz
import MDAnalysis as mda
from Bio.PDB import PDBParser
from Bio.PDB.DSSP import DSSP
from scipy.spatial import distance, ConvexHull
import os 

############################### MAIN FUNCTIONS ##################################################

##########################################################################################
# Building file name dictionary

def get_file_name_dict(probis_zip : zipfile.ZipFile):
    
    # in: PROBIS or PDB, out: fully structured file name dictionary 
    # (levels: pdb id -> chain -> bsid(filenamenoext) -> file names)

    def insert_into_pdb_dict(dict, pdb_id, chain_id, bsite_id, val):
        if (pdb_id in dict):
            if (chain_id in dict[pdb_id]):
                if (bsite_id in dict[pdb_id][chain_id]):
                    dict[pdb_id][chain_id][bsite_id] = dict[pdb_id][chain_id][bsite_id] + [val]
                else:
                    dict[pdb_id][chain_id][bsite_id] = [val]
            else:
                dict[pdb_id][chain_id] = {bsite_id : [val]}
        else:
            dict[pdb_id] = {chain_id : {bsite_id : [val]}}

    pdb_dict = {}
    no_ext_regex_grouped = re.compile('([a-z0-9]{4})_([a-zA-Z0-9]+)_([a-zA-Z]+_[0-9]+)')

    for name in probis_zip.namelist():

        no_ext_name = no_ext_regex_grouped.search(name)
        pdb_id = no_ext_name.group(1)
        chain_id = no_ext_name.group(2)
        bsite_id = no_ext_name.group(3)

        insert_into_pdb_dict(pdb_dict,  pdb_id, chain_id, bsite_id, name)

    return pdb_dict

def probis_process_pdbs(probis_zip, pdb_dict, pdb_ids = []):
    # model_name, cen_name = filegroup[0], filegroup[1]

    pdbs = pdb_dict if pdb_ids == [] else pdb_ids # this works...

    processed_elts = []
    for pdb_id in pdbs:
        for chain_id in pdb_dict[pdb_id]:
            bsites = [x for x in pdb_dict[pdb_id][chain_id]]

            gz_model_data = BytesIO(probis_zip.read(pdb_dict[pdb_id][chain_id][bsites[0]][0]))
            with gz.GzipFile(fileobj=gz_model_data) as gzfile:
                try:
                    rdkit_rec = merge_pdb_to_mol(gzfile.read().decode())
                except Exception as e:
                    print(f"RDKIT ERROR READING PDB FILE {pdb_id}_{chain_id}:")
                    print(e)
                    continue

            all_centroids = []
            centroids=[]
            per_bsite_results = []
            for bsite_id in bsites:
                gz_cen_data = BytesIO(probis_zip.read(pdb_dict[pdb_id][chain_id][bsite_id][1]))
                with gz.GzipFile(fileobj=gz_cen_data) as gzfile:
                    cen_file = gzfile.read().decode()
                
                centroids=list(pd.read_csv(StringIO(cen_file), sep='\t')[["x","y","z"]].values)
                all_centroids+=centroids
                
                try:
                    bsite_counts = probis_compute_bsite_counts(model=rdkit_rec, centroids=centroids, pdb_id = pdb_id, chain_id= chain_id, bsite_id = bsite_id)
                except Exception as e:
                    print(f"ERROR COMPUTING AA COUNTS FOR {pdb_id}_{chain_id}:")
                    print(e)
                    continue

                per_bsite_results.append(bsite_counts)
            
            # can skip computing this if 1 bsite 
            if len(per_bsite_results)==1:
                per_chain_result = per_bsite_results[0][0:2]+["ALL"]+per_bsite_results[0][3:]
                #print("Optimized")
            elif len(per_bsite_results)>1:
                per_chain_result = probis_compute_bsite_counts(model=rdkit_rec, centroids=all_centroids, pdb_id = pdb_id, chain_id= chain_id, bsite_id = "ALL")
            else:
                continue
            # treat the whole chain (combined bsites) as just another entry
            per_bsite_results.append(per_chain_result)
            processed_elts+=per_bsite_results

    return processed_elts

def probis_compute_bsite_counts(model, centroids, cutoff_low=2, cutoff=6, rasa_cutoff=0.25, pdb_id=None, chain_id=None, bsite_id = None):
    
    mda_rec = mda.Universe(model)
    rasa_dict = get_rasa_dict(model)

    sel_str = get_cen_sel_str(discriminate_loaded_centroids(centroids, mda_rec, cutoff_low), cutoff)

    all_bsite_atoms = mda_rec.select_atoms(sel_str)

    all_other_atoms = mda_rec.atoms - all_bsite_atoms
    
    bsite_surface_atoms = get_surf_atoms(all_bsite_atoms, rasa_dict, rasa_cutoff, mda_rec)
    bsite_buried_atoms = all_bsite_atoms - bsite_surface_atoms

    non_bsite_surface_atoms = get_surf_atoms(all_other_atoms, rasa_dict, rasa_cutoff, mda_rec)
    non_bsite_core_atoms = all_other_atoms - non_bsite_surface_atoms
    
    bsite_surf_counted = get_aa_counts(get_rasa_descriptor(bsite_surface_atoms.residues, rasa_dict), rasa_cutoff)
    bsite_core_counted = get_aa_counts(get_rasa_descriptor(bsite_buried_atoms.residues, rasa_dict), 0)
    other_surf_counted = get_aa_counts(get_rasa_descriptor(non_bsite_surface_atoms.residues, rasa_dict), rasa_cutoff)
    other_core_counted = get_aa_counts(get_rasa_descriptor(non_bsite_core_atoms.residues, rasa_dict), 0)
    total_counted = get_aa_counts(get_rasa_descriptor((mda_rec.atoms).residues, rasa_dict), 0)

    return [pdb_id, chain_id, bsite_id, bsite_surf_counted, bsite_core_counted, other_surf_counted, other_core_counted, total_counted] #

def probis_compute_final_counts(probis_zip, pdb_dict, pdb_ids=[]):
    rows_value_counts = probis_process_pdbs(probis_zip, pdb_dict, pdb_ids)
    rows_expanded = [expand_value_counts_row(x) for x in rows_value_counts]
    return probis_build_df(rows_expanded)

def probis_save_tsv(result_df, out_path):
    pdbs = list(set(result_df["pdb_id"]))
    pdbs.sort()
    result_df.to_csv(f"{out_path}/{pdbs[0]}_to_{pdbs[-1]}_counts.tsv", sep="\t")

    

############################### HELPER FUNCTIONS ##################################################

###############################################
# DataFrame handling

def expand_value_counts_row(row):
    all_aas=["ALA", "ARG", "ASN", "ASP", "CYS", "GLN", "GLU", "GLY", "HIS", "ILE", "LEU", "LYS", "MET", "PHE", "PRO", "SER", "THR", "TRP", "TYR", "VAL"]
    row_expanded = row[:3]
    for val_counts in row[3:]:
        row_expanded+=list(pd.Series(val_counts, index=all_aas).fillna(0).astype(int).values)
    return row_expanded

def probis_build_df(expanded_rows):
    all_aas=["ALA", "ARG", "ASN", "ASP", "CYS", "GLN", "GLU", "GLY", "HIS", "ILE", "LEU", "LYS", "MET", "PHE", "PRO", "SER", "THR", "TRP", "TYR", "VAL"]
    categories = ["bsite_surf", "bsite_core", "other_surf", "other_core", "total"]
    columns = ["pdb_id","chain_id","bsite_id"]+[f"{cat}_{aa}" for cat in categories for aa in all_aas]

    return pd.DataFrame(data=expanded_rows, columns=columns)


# ###########################################
# Merging multimodel pdb into a single object

def merge_mols(mollist):
        if mollist==[]: 
             print("Empty molecule!")
             return Chem.Mol()
        else:
            mergedmol = mollist[0]
            for mol in mollist[1:]:
                mergedmol = Chem.CombineMols(mergedmol, mol)
            return mergedmol
        
# if we have molecules to merge (1 protein and 1 or more cofactors), then merge
# find "MODEL", select it, select everything up to next MODEL

def merge_pdb_to_mol(pdbstring):
    #RDLogger.DisableLog('rdApp.*')
    block_start_pos_lst = [i for i in range(len(pdbstring)) if (pdbstring.startswith("MODEL", i))]
    mols=[]
    if len(block_start_pos_lst)>1:
        for i in range(len(block_start_pos_lst)-1):
            mols.append(AllChem.MolFromPDBBlock(pdbstring[block_start_pos_lst[i]: block_start_pos_lst[i+1]], sanitize=False, removeHs=False, proximityBonding=False)) # take blocks between all positions of MODEL
        mols.append(AllChem.MolFromPDBBlock(pdbstring[block_start_pos_lst[i+1]:], sanitize=False, removeHs=False, proximityBonding=False)) # from last position till the end
        return merge_mols(mols)
    else:
        return AllChem.MolFromPDBBlock(pdbstring, sanitize=False, removeHs=False, proximityBonding=False)

#############################################
# RASA

def get_rasa_dict(pdb_mol):
    dssp_string = Chem.rdmolfiles.MolToPDBBlock(pdb_mol)
    #problem - poorly formatted files such as: 4c0u 4c0y 4c10
    
    # Write tempfile to shared memory "/dev/shm" (this directory is treated as all others but exists solely in ram - faster!)
    with NamedTemporaryFile(delete=True, suffix=".pdb", prefix="bsr_", dir="/dev/shm") as tmp_pdb:
            
            tmp_pdb.write(dssp_string.encode())
            tmp_pdb.seek(0) #need to seek back to 0 after writing otherwise reading will be done from the last line (so we read nothing)
            # /\ not including the above line works for some proteins and not others, if correctness issues or crashes arise this should be revisited.
            #shutil.copy(tmp_pdb.name, './debug_pdb.pdb')
            structure = PDBParser().get_structure(id="NO_PDB_ID", file=tmp_pdb.name)
            model = structure[0]
            dssp = DSSP(model, tmp_pdb.name, dssp='mkdssp')

    rasa_dict = {}
    for key in dssp.keys():
            # only ever accessing single chain at once 
            # so resnum should be enough to uniquely identify residue
            rasa = dssp[key][3]
            if rasa != "NA":
                rasa_dict[key[1][1]]=rasa 
            
    return rasa_dict

def get_rasa_descriptor(res_group, rasa_dict):
    return [[res.resnum,
             res.resname, 
             rasa_dict[res.resnum]]
            for res in res_group if res.resnum in rasa_dict]

def get_surf_atoms(atoms : mda.AtomGroup, rasa_dict, rasa_cutoff, mda_rec):

    # with this, nonstandard residues are automatically not surface, though this shouldn't affect anything
    # as they aren't counted either way
  
    surf_atoms = []
    for a in atoms:
        if a.resnum in rasa_dict:
            dict_val = rasa_dict[a.resnum]
            if not isinstance(dict_val, str):
                if dict_val>=rasa_cutoff:
                    surf_atoms.append(a)
            else:
                print(rasa_dict[a.resnum])

    if surf_atoms==[]:
        return mda_rec.select_atoms("")
    return mda.AtomGroup(surf_atoms)

#######################################################
# Centroids

def get_cen_sel_str(cen_vals, cutoff):

    string = "byres "
    for xyz in cen_vals:
        string += f"(point {xyz[0]} {xyz[1]} {xyz[2]} {cutoff}) or "

    string = "" if string=="byres " else string[:-4] # remove " or" or create empty selection string
    return string


def discriminate_loaded_centroids(cen_vals, rec, cutoff_low=2):

    good_cens = []
    string = ""
    for coords_list in cen_vals:
        coords = str(coords_list[0])+" "+ str(coords_list[1])+" "+ str(coords_list[2])
        string = "(point "+coords+" "+str(cutoff_low)+")"
        if len(rec.select_atoms(string))==0:
            good_cens.append(coords_list)
        #else:
            #print("removed bad cen " + str(coords_list))

    return good_cens

############################
# AA counts

def get_aa_counts(desc, rasa_cutoff):
    return pd.DataFrame([x[1] for x in desc if x[2]>=rasa_cutoff],columns=['res_name'])['res_name'].value_counts()