from io import BytesIO, StringIO
import re
from tempfile import NamedTemporaryFile
import warnings
import base_defs as base
import zipfile
import numpy as np
import pandas as pd
from rdkit import Chem
from rdkit.Chem import AllChem
import gzip as gz
import MDAnalysis as mda
from Bio.PDB import PDBParser
from Bio.PDB.DSSP import DSSP
from scipy.spatial import distance, ConvexHull
import os 


############################### MAIN FUNCTIONS ##################################################

##########################################################################################
# Building file name dictionary

def get_file_name_dict(probis_zip : zipfile.ZipFile):
    
    # in: PROBIS or PDB, out: fully structured file name dictionary 
    # (levels: pdb id -> chain -> bsid(filenamenoext) -> file names)

    def insert_into_pdb_dict(dict, pdb_id, chain_id, bsite_id, val):
        if (pdb_id in dict):
            if (chain_id in dict[pdb_id]):
                if (bsite_id in dict[pdb_id][chain_id]):
                    dict[pdb_id][chain_id][bsite_id] = dict[pdb_id][chain_id][bsite_id] + [val]
                else:
                    dict[pdb_id][chain_id][bsite_id] = [val]
            else:
                dict[pdb_id][chain_id] = {bsite_id : [val]}
        else:
            dict[pdb_id] = {chain_id : {bsite_id : [val]}}

    pdb_dict = {}
    no_ext_regex_grouped = re.compile('([a-z0-9]{4})_([a-zA-Z0-9]+)_([a-zA-Z]+_[0-9]+)')

    for name in probis_zip.namelist():

        no_ext_name = no_ext_regex_grouped.search(name)
        pdb_id = no_ext_name.group(1)
        chain_id = no_ext_name.group(2)
        bsite_id = no_ext_name.group(3)

        insert_into_pdb_dict(pdb_dict,  pdb_id, chain_id, bsite_id, name)

    return pdb_dict

def probis_process(probis_zip, pdb_dict, targets=[]):
    # model_name, cen_name = filegroup[0], filegroup[1]
    if targets==[]:
        pdb_dict_subset = pdb_dict
    else:
        pdb_dict_subset = {pdb_id : pdb_dict[pdb_id] for pdb_id in targets}

    processed_elts = []
    for pdb_id in pdb_dict_subset:
        for chain_id in pdb_dict_subset[pdb_id]:
            try:
                #print(pdb_id)
                #print(chain_id)
                bsites = [bsite_id for bsite_id in pdb_dict_subset[pdb_id][chain_id]]

                gz_model_data = BytesIO(probis_zip.read(pdb_dict_subset[pdb_id][chain_id][bsites[0]][0]))
                with gz.GzipFile(fileobj=gz_model_data) as gzfile:
                    rdkit_rec = base.merge_pdb_to_mol(gzfile.read().decode())
                    rasa_dict = base.get_rasa_dict(rdkit_rec)
                    mda_rec = mda.Universe(rdkit_rec)

                for bsite_id in bsites:
                    gz_cen_data = BytesIO(probis_zip.read(pdb_dict_subset[pdb_id][chain_id][bsite_id][1]))
                    with gz.GzipFile(fileobj=gz_cen_data) as gzfile:
                        cen_file = gzfile.read().decode()
                    
                    centroids = pd.read_csv(StringIO(cen_file), sep='\t')[["x","y","z"]].values

                    full_id = ".".join([pdb_id, chain_id, bsite_id])

                    res = compute_features(mda_rec=mda_rec, centroids=centroids, 
                                        rasa_dict=rasa_dict, id=full_id)
                    
                    processed_elts.append(res)
            except Exception as e:
                print("Error with: "+pdb_id+" "+chain_id+": ")
                print(e)

    res_df = pd.DataFrame(processed_elts)
    res_df[["pdb_id", "chain_id", "bsite_id"]] = res_df["id"].str.split('.', n=2, expand=True)
    res_df = res_df.drop("id", axis=1)
    return res_df[["pdb_id","chain_id","bsite_id","avg_bsite_rasa","top_aas","num_bsite_aa","num_other_aa", "bsite_hull_volume","total_hull_volume"]]
                

def compute_features(mda_rec, rasa_dict, centroids, cutoff=10, 
                     cutoff_low=2, id=None):
    
    valid_centroids = discriminate_loaded_centroids(centroids, mda_rec, cutoff_low)
    sel_str = base.get_cen_sel_str(valid_centroids, cutoff)

    bsite_atoms = mda_rec.select_atoms(sel_str)
    other_atoms = mda_rec.atoms - bsite_atoms
    
    bsite_rasa_desc = base.get_rasa_descriptor(bsite_atoms.residues, rasa_dict)

    res_dist_desc = base.get_res_dist_desc(bsite_atoms, valid_centroids, cutoff)

    top_aas = res_dist_desc[:5]["res_str"].values

    avg_bsite_rasa = round(np.average([x[2] for x in bsite_rasa_desc]), 2)

    try: bsite_hull_volume = round(ConvexHull(bsite_atoms.positions).volume, 2)
    except: bsite_hull_volume = 0
    
    total_hull_volume = round(ConvexHull(mda_rec.atoms.positions).volume, 2)

    res = {
        "id": id,
        "bsite_hull_volume":bsite_hull_volume,
        "total_hull_volume": total_hull_volume,
        "avg_bsite_rasa": avg_bsite_rasa,
        "top_aas":top_aas,
        "num_bsite_aa": len(bsite_atoms.residues), 
        "num_other_aa": len(other_atoms.residues)
    } 
      
    # if (not stats_only):
    #     res.update({  
    #         "res_dist_desc": res_dist_desc,
    #         "total": mda_rec.atoms, 
    #         "bsite": bsite_atoms, 
    #         "other": other_atoms, 
    #         #"desc":desc
    #         })
        
    return res

def save_stats_tsv_by_pdb_dict(probis_zip, pdb_dict, out_path, targets):
    
    res_df = pd.DataFrame(probis_process(probis_zip=probis_zip, pdb_dict=pdb_dict, targets=targets))

    key_list = targets
    key_list.sort()
    
    res_df.to_csv(f"{out_path}/{key_list[0]}_to_{key_list[-1]}_stats.tsv", sep="\t")



def debug_return_intermediate(zip_path, pdb_ids):
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        with zipfile.ZipFile(zip_path, 'r') as probis_zip:
            pdb_dict = get_file_name_dict(probis_zip)#,"3pu0","1ar6","1a1r"])
            probis_process_res = probis_process(probis_zip, pdb_dict, pdb_ids)
    
    return [pdb_dict, probis_process_res]


############################### HELPER FUNCTIONS ##################################################

def discriminate_loaded_centroids(cen_vals, rec, cutoff_low=2):

    good_cens = []
    string = ""
    for coords_list in cen_vals:
        coords = str(coords_list[0])+" "+ str(coords_list[1])+" "+ str(coords_list[2])
        string = "(point "+coords+" "+str(cutoff_low)+")"
        if len(rec.select_atoms(string))==0:
            good_cens.append(coords_list)
        # else:
        #     print("removed bad cen " + str(coords_list))

    return good_cens