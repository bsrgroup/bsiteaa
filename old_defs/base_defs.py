from io import BytesIO, StringIO
import re
from tempfile import NamedTemporaryFile
import zipfile
import numpy as np
import pandas as pd
from rdkit import Chem
from rdkit.Chem import AllChem
import gzip as gz
import MDAnalysis as mda
from Bio.PDB import PDBParser
from Bio.PDB.DSSP import DSSP
from scipy.spatial import distance, ConvexHull
import os 

##########################################################################################
# Building file name dictionary 

def get_file_name_dict(probis_zip : zipfile.ZipFile, pdb_ids_from_argument : list = ["ALL_PDBS"] ):
    no_ext_regex = re.compile('[a-z0-9]{4}_[a-zA-Z0-9]+_[a-zA-Z]+_[0-9]+')
    names = probis_zip.namelist() # assumption - this list has consistent order for every probis file
    pdb_dict = {}
    for name in names:
        pdb_id = no_ext_regex.search(name)[0][0:4]
        if pdb_id in pdb_ids_from_argument or pdb_ids_from_argument==["ALL_PDBS"]:
            if pdb_id in pdb_dict: # if the pdbid we're reading is already present (fast check) append another element to the list of files
                pdb_dict.update({pdb_id: pdb_dict.get(pdb_id)+[name]}) 
            else:
                pdb_dict.update({pdb_id: [name]})
    
    def inner_file_matrix(file_list):
        common_names = {no_ext_regex.search(fname)[0] for fname in file_list}
        return np.array(file_list).reshape(len(common_names), 4)
    
    # now just set each element's value to this categorized matrix instead of the lists we processed
    for key in pdb_dict:
        pdb_dict.update( {key : inner_file_matrix(pdb_dict.get(key))} )
    return pdb_dict


##########################################################################################
# Merging multimodel pdb into a single object
def merge_mols(mollist):
        if mollist==[]: 
             print("Empty molecule!")
             return Chem.Mol()
        else:
            mergedmol = mollist[0]
            for mol in mollist[1:]:
                mergedmol = Chem.CombineMols(mergedmol, mol)
            return mergedmol
        # if we have molecules to merge (1 protein and 1 or more cofactors), then merge
        # find "MODEL", select it, select everything up to next MODEL
        
def merge_pdb_to_mol(pdbstring):
    #RDLogger.DisableLog('rdApp.*')
    block_start_pos_lst = [i for i in range(len(pdbstring)) if (pdbstring.startswith("MODEL", i))]
    #print(block_start_pos_lst)
    mols=[]
    if len(block_start_pos_lst)>1:
        for i in range(len(block_start_pos_lst)-1):
            mols.append(AllChem.MolFromPDBBlock(pdbstring[block_start_pos_lst[i]: block_start_pos_lst[i+1]], sanitize=False, removeHs=False, proximityBonding=False)) # take blocks between all positions of MODEL
        mols.append(AllChem.MolFromPDBBlock(pdbstring[block_start_pos_lst[i+1]:], sanitize=False, removeHs=False, proximityBonding=False)) # from last position till the end
        #RDLogger.EnableLog('rdApp.*')
        return merge_mols(mols)
    else:
        merged=AllChem.MolFromPDBBlock(pdbstring, sanitize=False, removeHs=False, proximityBonding=False)
        #RDLogger.EnableLog('rdApp.*')
        return merged

            

##########################################################################################
# Building selection strings
# Can we dynamically discriminate certain centroids by having the selection be (point xyz 10 and not ANY point xyz 2)?

# def get_centroids(cen_file):
#     #print(cen_file)
#     cenvals = pd.read_csv(StringIO(cen_file), sep='\t')
#     return cenvals

def get_cen_sel_str(cen_vals, cutoff):

    #cen_vals = discriminate_centroids(cen_file, rec, cutoff_low)

    string = "byres "
    for coords_list in cen_vals:
        coords = str(coords_list[0])+" "+ str(coords_list[1])+" "+ str(coords_list[2])
        string += "(point "+coords+" "+str(cutoff)+") "+"or "
    if string!="byres ": string = string[:-4]
    else: string = ""

    return string

def discriminate_centroids(cen_file, rec, cutoff_low):
    cen_vals = pd.read_csv(StringIO(cen_file), sep='\t')[["x","y","z"]].values
    
    good_cens = []
    string = ""
    for coords_list in cen_vals:
        coords = str(coords_list[0])+" "+ str(coords_list[1])+" "+ str(coords_list[2])
        string = "(point "+coords+" "+str(cutoff_low)+")"
        if len(rec.select_atoms(string))==0:
            good_cens.append(coords_list)
        # else:
        #     print("removed bad cen " + str(coords_list))

    return good_cens
#################################################################
# RASA


    
def get_rasa_dict(pdb_mol):
    dssp_string = Chem.rdmolfiles.MolToPDBBlock(pdb_mol)
    #problem - poorly formatted files such as: 4cou 4coy 4c10
    
    # Write tempfile to shared memory "/dev/shm" (this directory is treated as all others but exists solely in ram - faster!)
    with NamedTemporaryFile(delete=True, suffix=".pdb", prefix="bsr_", dir="/dev/shm") as tmp_pdb:
            
            tmp_pdb.write(dssp_string.encode())
            tmp_pdb.seek(0) #need to seek back to 0 after writing otherwise reading will be done from the last line (so we read nothing)
            # /\ not including the above line works for some proteins and not others, if correctness issues or crashes arise this should be revisited.
            #shutil.copy(tmp_pdb.name, './debug_pdb.pdb')
            structure = PDBParser().get_structure(id="NO_PDB_ID", file=tmp_pdb.name)
            model = structure[0]
            #dssp = DSSP(model, tmp_pdb.name, dssp='/home/andrej/Desktop/dssp-2.3.0/mkdssp')
            dssp = DSSP(model, tmp_pdb.name, dssp='mkdssp')

    dict = {}
    for key in dssp.keys():
            #print(key)
            dict[key[1][1]]=dssp[key][3] # KEY ASSUMPTION (but likely true) - IN A PDB FILE RESNUM IS NON-OVERLAPPING BETWEEN CHAINS!
    return dict

##########################################################################################
# Loading file groups

# A file group shares the "common name", that is the 
# `"<pdbid>_<moltype>_<id>"` part of 
# `"filetype_pdbid_moltype_id_extension"` which is the format uniquely identifying all files.

def count_bsites(pdb_id, pdb_dict):
    no_ext_regex = re.compile('[a-z0-9]{4}_[a-zA-Z0-9]+_[a-zA-Z]+_[0-9]+')
    sum=0
    for filegroup in pdb_dict[pdb_id]:
        if "cofactor" not in str(filegroup[0]):
            sum+=1
    return sum


def get_surf_atoms(atoms : mda.AtomGroup, rasa_dict, rasa_cutoff, mda_rec):
    
    surf_atoms = []
    for a in atoms:
        try:
            if rasa_dict[a.resnum]>=rasa_cutoff:
                surf_atoms.append(a)
                #print("AAA")
        except Exception as e:
            pass # with this, nonstandard residues are automatically not surface, though this shouldn't affect anything

    #print(mda.AtomGroup(surf_atoms))   
    if surf_atoms==[]:
        return mda_rec.select_atoms("")
    return mda.AtomGroup(surf_atoms)

def get_chain_dict(filegroups):
    find_chain_regex = re.compile('[a-z0-9]{4}_([a-zA-Z0-9]+)_[a-zA-Z]+_[0-9]+')
    by_chain_dict = {}
    for filegroup in filegroups:
        chain = find_chain_regex.search(filegroup[0]).group(1)
        if chain in by_chain_dict:
            by_chain_dict[chain] = by_chain_dict[chain]+[filegroup]
        else:
            by_chain_dict[chain] = [filegroup]
    return by_chain_dict
            
def process_chain(chaingroup, probis_zip, cutoff=10, cutoff_low=2, rasa_cutoff=0.25):
    #a chain group is a set of file groups, arranged by chain
    #pick any receptor from the chain group, they only differ by cofactors anyways
    #load all centroids, build collective selection string
    #select all bsites from arbitrary receptor simultaneously
    #proceed as with process_file_group

    
    gzippeddata = BytesIO(probis_zip.read(chaingroup[0][0]))
    with gz.GzipFile(fileobj=gzippeddata) as gzfile:
        rdkit_rec = merge_pdb_to_mol(gzfile.read().decode()) # read the single, first receptor file.

    loaded_cen_files = []
    for filegroup in chaingroup:
        gzippeddata = BytesIO(probis_zip.read(filegroup[1])) # read the cen file
        with gz.GzipFile(fileobj=gzippeddata) as gzfile:
            loaded_cen_files.append(gzfile.read().decode())


    mda_rec = mda.Universe(rdkit_rec)
    rasa_dict = get_rasa_dict(rdkit_rec)
            
    sel_str = "byres "+" or ".join([x for x in [get_cen_sel_str(discriminate_centroids(cenfile,mda_rec,cutoff_low), cutoff, )[5:] for cenfile in loaded_cen_files] if x != ""])
    if sel_str=="byres ": sel_str = ""
    #print(sel_str)

    try:
        all_bsite_atoms = mda_rec.select_atoms(sel_str)
    except Exception as e:
        print("Failed: "+sel_str)
        raise e

    all_other_atoms = mda_rec.atoms - all_bsite_atoms
    
    bsite_surface_atoms = get_surf_atoms(all_bsite_atoms, rasa_dict, rasa_cutoff, mda_rec)
    bsite_buried_atoms = all_bsite_atoms - bsite_surface_atoms

    non_bsite_surface_atoms = get_surf_atoms(all_other_atoms, rasa_dict, rasa_cutoff, mda_rec)
    non_bsite_core_atoms = all_other_atoms - non_bsite_surface_atoms
    
    bsite_surf_counted = get_aa_counts(get_rasa_descriptor(bsite_surface_atoms.residues, rasa_dict), rasa_cutoff)
    bsite_core_counted = get_aa_counts(get_rasa_descriptor(bsite_buried_atoms.residues, rasa_dict), 0)
    other_surf_counted = get_aa_counts(get_rasa_descriptor(non_bsite_surface_atoms.residues, rasa_dict), rasa_cutoff)
    other_core_counted = get_aa_counts(get_rasa_descriptor(non_bsite_core_atoms.residues, rasa_dict), 0)
    total_counted = get_aa_counts(get_rasa_descriptor((mda_rec.atoms).residues, rasa_dict), 0)

    return [bsite_surf_counted, bsite_core_counted, other_surf_counted, other_core_counted, total_counted]


# def collect_chain_by_filegroup(chaingroup, probis_zip, cutoff=10, cutoff_low=2, cb_only = False, stats_only=False, num_top_aas = 4):
#     #a chain group is a set of file groups, arranged by chain
#     #pick any receptor from the chain group, they only differ by cofactors anyways
#     #load all centroids, build individual selection strings
#     #select all bsites separately
#     #proceed as with process_file_group

    
#     gzippeddata = BytesIO(probis_zip.read(chaingroup[0][0]))
#     with gz.GzipFile(fileobj=gzippeddata) as gzfile:
#         rdkit_rec = merge_pdb_to_mol(gzfile.read().decode()) # read the single, first receptor file.

#     mda_rec = mda.Universe(rdkit_rec)
#     if cb_only:
#          # can select only CB or all atoms
#          mda_rec = mda_rec.select_atoms("name CB")
#     rasa_dict = get_rasa_dict(rdkit_rec)

#     counted_by_filegroup = []

#     #loaded_cen_files = []
#     for i, filegroup in enumerate(chaingroup):
#         gzippeddata = BytesIO(probis_zip.read(filegroup[1])) # read the cen file
#         with gz.GzipFile(fileobj=gzippeddata) as gzfile:
#             loaded_cen_file = gzfile.read().decode()
            
#         valid_centroids = discriminate_centroids(loaded_cen_file, mda_rec, cutoff_low)
#         sel_str = get_cen_sel_str(valid_centroids, cutoff)

#         if "(" not in sel_str: 
#             sel_str = ""

#         try:
#             bsite_atoms = mda_rec.select_atoms(sel_str)
#         except Exception as e:
#             print("Failed: "+sel_str)
#             raise e
        
#         other_atoms = mda_rec.atoms - bsite_atoms

#         bsite_desc = get_stats_descriptor(bsite_atoms.residues, rasa_dict)

#         avg_bsite_rasa = round(np.average([x[2] for x in bsite_desc]), 2)
#         #

#         desc = get_res_dist_desc(bsite_atoms, valid_centroids, cutoff)
#         top_AAs = desc[:num_top_aas]["res_str"].values
#         top_AAs.sort()

#         try:
#             bsite_hull_volume = round(ConvexHull(bsite_atoms.positions).volume, 2)
#         except:
#             bsite_hull_volume = 0
        
#         total_hull_volume = round(ConvexHull(mda_rec.atoms.positions).volume, 2)
        

#         res = {
#                 "bsite_index": i,
#                 "bsite_hull_volume":bsite_hull_volume,
#                 "total_hull_volume": total_hull_volume,
#                 "avg_bsite_rasa": avg_bsite_rasa,
#                 "top_AAs":top_AAs,
#                 "num_bsite_aa": len(bsite_atoms.residues), 
#                 "num_other_aa": len(other_atoms.residues)
#             } 

       
#         if (not stats_only):
#             res.update({  
#                 "total": mda_rec.atoms, 
#                 "bsite": bsite_atoms, 
#                 "other": other_atoms, 
#                 #"desc":desc
#                 })
            
#         counted_by_filegroup.append(res)
#     return counted_by_filegroup

# def collect_stats_for_pdbs(pdb_dict, probis_zip, cutoff, num_top_aas):

#     all_pdb_dfs = []
#     for pdb_id in pdb_dict:
#         chain_dict = get_chain_dict(pdb_dict[pdb_id])

#         res=pd.DataFrame()
#         for chain_id in chain_dict:

#             row = pd.DataFrame(
#             collect_chain_by_filegroup(chain_dict[chain_id], probis_zip, cutoff, cb_only=False, stats_only=True, num_top_aas=num_top_aas)
#             )
#             row["chain_id"]=chain_id
#             row["pdb_id"]=pdb_id
#             res = pd.concat([res, row])
#         all_pdb_dfs.append(res)

#     return pd.concat(all_pdb_dfs)[["pdb_id", "chain_id", "bsite_index", "top_AAs", "bsite_hull_volume", "total_hull_volume", "avg_bsite_rasa", "num_bsite_aa", "num_other_aa"]]


def get_res_dist_desc(bsite_atoms, centroids, cutoff):
    desc = []
    for cencoords in centroids: # we get a centroid
        for res in bsite_atoms.residues:
            dist = min([distance.euclidean(cencoords, atom_pos) for atom_pos in res.atoms.positions])
            if (dist<=cutoff):
                desc.append([res, dist])
    

    if (len(bsite_atoms)>0 and desc != []):
        df_desc = pd.DataFrame(desc, columns=["resobj","dist"])

        #  better idea: Mold to a .csv string, make loading easier
        df_desc=df_desc.groupby('resobj').agg({'dist': lambda x: (len(list(x)), sorted(list(x)))})
        df_desc[['no_picked', 'dist_list']] = pd.DataFrame(df_desc['dist'].tolist(), index=df_desc.index)
        df_desc = df_desc.drop('dist', axis=1).reset_index()
        # the following line reduces residue objects to strings to fix a bug and enable saving.
        df_desc[['res_id', 'res_num', 'res_str']] = pd.DataFrame(df_desc['resobj'].map(lambda x: [x.resid, x.resnum,str(x)[9:12]]).tolist())

        df_desc = df_desc[['res_num', 'res_str', 'no_picked', 'dist_list']]
    else:
        df_desc = pd.DataFrame(columns=['res_num', 'res_str', 'no_picked', 'dist_list'])

    df_desc.sort_values(axis=0, by='dist_list', ascending=True, kind='stable', key=lambda col: col.map(lambda x: x[0]), inplace=True)
    return df_desc
    
        
def process_file_groups_by_chain(pdb_id, pdb_dict, probis_zip, cutoff, cutoff_low=2, rasa_cutoff=0.25):
    chain_dict = get_chain_dict(pdb_dict[pdb_id])
    reshaped_counts = list(zip(*[(a,b,c,d,e) for a,b,c,d,e in [process_chain(chain_dict[chain], probis_zip, cutoff, cutoff_low, rasa_cutoff) for chain in chain_dict]]))
    return [sum_counts(reshaped_counts[0]), sum_counts(reshaped_counts[1]), sum_counts(reshaped_counts[2]), sum_counts(reshaped_counts[3]), sum_counts(reshaped_counts[4]), len(pdb_dict[pdb_id])] 


################################################################################
# Building stats dataframe

def try_dict(rasa_dict, x):
        # we try-catch relative asa dictionary access as it isn't computed for nonstandard residues
        try:
            if rasa_dict[x]!="NA":
                return rasa_dict[x]
            # else:
            #     print("Caught!")
            #     for res in res_group:
            #         if res.resnum==x:
            #             print(res)
        except:
            return None

def get_rasa_descriptor(res_group, rasa_dict):

    return [[res.resnum, str(res)[9:12], try_dict(rasa_dict, res.resnum)] for res in res_group if try_dict(rasa_dict, res.resnum)!=None]

def get_aa_counts_inverse_cutoff(desc, rasa_cutoff):
        #all_AAs =["ALA", "ARG", "ASN", "ASP", "CYS", "GLN", "GLU", "GLY", "HIS", "ILE", "LEU", "LYS", "MET", "PHE", "PRO", "SER", "THR", "TRP", "TYR", "VAL"]
    try:
        #print([x[1] for x in desc if x[2]>=rasa_cutoff])
        
        return pd.DataFrame([x[1] for x in desc if x[2]<rasa_cutoff], columns=['res_name'])['res_name'].value_counts()
    except Exception as e:
        #print(desc)
        raise e

def get_aa_counts(desc, rasa_cutoff):
    #all_AAs =["ALA", "ARG", "ASN", "ASP", "CYS", "GLN", "GLU", "GLY", "HIS", "ILE", "LEU", "LYS", "MET", "PHE", "PRO", "SER", "THR", "TRP", "TYR", "VAL"]
    try:
        #print([x[1] for x in desc if x[2]>=rasa_cutoff])
        
        return pd.DataFrame([x[1] for x in desc if x[2]>=rasa_cutoff], columns=['res_name'])['res_name'].value_counts()
    except Exception as e:
        #print(desc)
        raise e
    
# def get_aas(desc, rasa_cutoff):
#     try:
#         return pd.DataFrame([(x[1], x[2]>=rasa_cutoff) for x in desc], columns=['res_name', 'on_surface'])
#     except:
#         print(desc)
#         raise ValueError("!?")

def sum_counts(count_list):
    all_AAs = ["ALA", "ARG", "ASN", "ASP", "CYS", "GLN", "GLU", "GLY", "HIS", "ILE", "LEU", "LYS", "MET", "PHE", "PRO", "SER", "THR", "TRP", "TYR", "VAL"]
    sum = pd.Series(data=[0]*20, index=all_AAs)
    for elt in count_list:
        sum = sum.add(elt, fill_value=0)
    return sum


################################################################################
## Saving a dataframe of AA counts by pdb and numbers of compounds

def get_counts_by_pdb(probis_zip, pdb_dict):
    
    counts_by_pdb = pd.DataFrame(columns=["pdb_id", "bsite_surf", "bsite_core", "other_surf", "other_core", "total", "num_bsites", "num_chains"])
    for i, pdb_id in enumerate(pdb_dict):
        try:
            num_chains = len(get_chain_dict(pdb_dict[pdb_id]))
            result = process_file_groups_by_chain(pdb_id, pdb_dict, probis_zip, cutoff = 6, cutoff_low = 2, rasa_cutoff = 0.25)
            counts_by_pdb.loc[len(counts_by_pdb)] = pd.Series({'pdb_id':pdb_id, 
                                                            "bsite_surf": result[0],
                                                            "bsite_core": result[1], 
                                                            "other_surf": result[2],
                                                            "other_core": result[3],
                                                            "total": result[4], 
                                                            "num_bsites": result[5], 
                                                            "num_chains": num_chains})
        except Exception as e:
            print("Error processing "+pdb_id+": ")
            print(e)
    return counts_by_pdb


# def save_by_pdb_dict(probis_zip, pdb_dict, out_path):
#     # obtain data frame of categorized counts per pdb
#     counts_by_pdb = get_counts_by_pdb(probis_zip, pdb_dict)
#     # name and save the file containing the dataframe
#     key_list = list(pdb_dict.keys())
#     key_list.sort()
#     counts_by_pdb.to_pickle(f"{out_path}/{key_list[0]}_to_{key_list[-1]}_counts.pkl")

# def get_tsv_from_pkl(path):
#     files = os.listdir(path)
#     all_AAs=["ALA", "ARG", "ASN", "ASP", "CYS", "GLN", "GLU", "GLY", "HIS", "ILE", "LEU", "LYS", "MET", "PHE", "PRO", "SER", "THR", "TRP", "TYR", "VAL"]
#     categories = ["bsite_surf","bsite_core","other_surf", "other_core", "total"]
#     counts_by_pdb=pd.concat([pd.read_pickle(path+file) for file in files])
#     for cat in categories:
#         counts_by_pdb[[cat+"_"+aa for aa in all_AAs]] = pd.DataFrame([pd.Series(row[cat], index=all_AAs).infer_objects(copy=False).fillna(0).astype(int) for i, row in counts_by_pdb.iterrows()])
#     return counts_by_pdb.drop(categories, axis=1).set_index("pdb_id").sort_index()
    
def save_tsv_by_pdb_dict(probis_zip, pdb_dict, out_path):
    all_AAs=["ALA", "ARG", "ASN", "ASP", "CYS", "GLN", "GLU", "GLY", "HIS", "ILE", "LEU", "LYS", "MET", "PHE", "PRO", "SER", "THR", "TRP", "TYR", "VAL"]
    categories = ["bsite_surf","bsite_core","other_surf", "other_core", "total"]
    # obtain data frame of categorized counts per pdb
    counts_by_pdb = get_counts_by_pdb(probis_zip, pdb_dict)
    # name and save the file containing the dataframe
    key_list = list(pdb_dict.keys())
    key_list.sort()
    for cat in categories:
        counts_by_pdb[[cat+"_"+aa for aa in all_AAs]] = pd.DataFrame([pd.Series(row[cat], index=all_AAs).infer_objects(copy=False).fillna(0).astype(int) for i, row in counts_by_pdb.iterrows()])
    counts_by_pdb=counts_by_pdb.drop(categories, axis=1).set_index("pdb_id")
    counts_by_pdb.to_csv(f"{out_path}/{key_list[0]}_to_{key_list[-1]}_counts.tsv", sep="\t")



#####################################################################################
## Loading and summing dataframes

def get_sum_from_tsv(path):
    files = os.listdir(path)
    #all_AAs=["ALA", "ARG", "ASN", "ASP", "CYS", "GLN", "GLU", "GLY", "HIS", "ILE", "LEU", "LYS", "MET", "PHE", "PRO", "SER", "THR", "TRP", "TYR", "VAL"]
    categories = ["bsite_surf","bsite_core","other_surf", "other_core", "total"]
    per_pdb_df = pd.concat([pd.read_csv(path+file, sep="\t") for file in files])

    return pd.DataFrame([pd.Series(per_pdb_df.filter(regex=cat+".*").sum().rename(index=lambda x: x[-3:])) for cat in categories]).transpose().set_axis(categories, axis=1)


#######################################################
# Amino acid categorization
def get_residue_categories():
    residue_categories = {}

    nonpolar_aliphatic = ["GLY","ALA","VAL","LEU","MET","ILE"]
    aromatic = ["PHE","TYR","TRP"]
    polar_uncharged = ["SER", "THR", "CYS", "PRO", "ASN", "GLN"]
    positively_charged = ["LYS","ARG","HIS"]
    negatively_charged = ["ASP","GLU"]

    [residue_categories.update({elt:"nonpolar_aliphatic"}) for elt in nonpolar_aliphatic]
    [residue_categories.update({elt:"aromatic"}) for elt in aromatic]
    [residue_categories.update({elt:"polar_uncharged"}) for elt in polar_uncharged]
    [residue_categories.update({elt:"positively_charged"}) for elt in positively_charged]
    [residue_categories.update({elt:"negatively_charged"}) for elt in negatively_charged]

    return residue_categories


def get_regions_for_chain(chaingroup, probis_zip, cutoff=10, cutoff_low=2, rasa_cutoff=0.25):
    gzippeddata = BytesIO(probis_zip.read(chaingroup[0][0]))
    with gz.GzipFile(fileobj=gzippeddata) as gzfile:
        rdkit_rec = merge_pdb_to_mol(gzfile.read().decode()) # read the single, first receptor file.

    loaded_cen_files = []
    for filegroup in chaingroup:
        gzippeddata = BytesIO(probis_zip.read(filegroup[1])) # read the cen file
        with gz.GzipFile(fileobj=gzippeddata) as gzfile:
            loaded_cen_files.append(gzfile.read().decode())

    mda_rec = mda.Universe(rdkit_rec)
    rasa_dict = get_rasa_dict(rdkit_rec)
            
    sel_str = "byres "+" or ".join([x for x in [get_cen_sel_str(discriminate_centroids(cenfile, mda_rec, cutoff_low), cutoff)[5:] for cenfile in loaded_cen_files] if x != ""])
    if sel_str=="byres ": sel_str = ""

    try:
        all_bsite_atoms = mda_rec.select_atoms(sel_str)
    except Exception as e:
        print("Failed: "+sel_str)
        raise e

    all_other_atoms = mda_rec.atoms - all_bsite_atoms
    
    bsite_surface_atoms = get_surf_atoms(all_bsite_atoms, rasa_dict, rasa_cutoff, mda_rec)
    bsite_buried_atoms = all_bsite_atoms - bsite_surface_atoms

    non_bsite_surface_atoms = get_surf_atoms(all_other_atoms, rasa_dict, rasa_cutoff, mda_rec)
    non_bsite_core_atoms = all_other_atoms - non_bsite_surface_atoms

    data = [bsite_surface_atoms, bsite_buried_atoms, non_bsite_surface_atoms, non_bsite_core_atoms]

    return pd.Series(data, index=["bsite_surf","bsite_core","other_surf","other_core"])