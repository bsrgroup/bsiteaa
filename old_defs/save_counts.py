import argparse
import os
import time
import warnings
import zipfile
import base_defs as base


##########################################################################################
#Initial global vars from command line

parser = argparse.ArgumentParser()
parser.add_argument('zip_path', nargs=1, default=None)
parser.add_argument('out_path', nargs=1, default=None)
parser.add_argument('pdb_ids', nargs='+', default=[])
args = parser.parse_args()

zip_path = args.zip_path[0]
out_path = args.out_path[0]
pdb_ids_from_argument = args.pdb_ids


##########################################################################################
# Running the script

if not os.path.exists(out_path):
    os.makedirs(out_path)
            
start_time=time.time()
with warnings.catch_warnings(): 
        warnings.simplefilter("ignore")
        with zipfile.ZipFile(zip_path, 'r') as probis_zip:
            pdb_dict = base.get_file_name_dict(probis_zip, pdb_ids_from_argument)
            base.save_tsv_by_pdb_dict(probis_zip, pdb_dict, out_path)

print("Done, saved " +str(len(pdb_dict))+ " pdbs in "+ str(round(time.time()-start_time, 1))+" seconds.")
