#!/bin/bash
SECONDS=0
echo 'Deleting old output'
rm -r ../out
mkdir ../out
echo 'Done, running parallel script'

cat ../virus_input.txt | parallel --joblog ../out/parallel_log_bsiteaa_virus_stats.txt -N50 python3 save_stats.py ../ProBiS_Dock_DB_bs_id1division_id9.zip ../out/bsiteaa_virus_out_stats/ >> ../out/command_log_bsiteaa_virus_stats.txt 2>&1

echo "Done, compressing files."

python3 -c "import shutil; shutil.make_archive('../out/bsiteaa_virus_out_stats_archive', 'zip', '../out/bsiteaa_virus_out_stats', '.')"

echo "Done, archive can now be scp-ed"

duration=$SECONDS
echo "Time taken: $((duration / 60)) minutes and $((duration % 60)) seconds."