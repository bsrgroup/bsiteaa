# Binding site amino acid composition analysis

## Author
Andrej Milisavljević - 89201188@student.upr.si

## Purpose
Code written for running analyses required for writing my diploma paper - "In silico analysis of the amino acid composition of binding sites on viral proteins"